package sical

import javax.transaction.Transaction
import javax.transaction.Transactional

class AlumnoController {

    static allowedMethods=[save: "POST"]

    def index() {
        respond Alumno.list()

    }
    def crear() { }

    @Transactional
    def save(Alumno alumno) {
            alumno.save(flush: true)
            redirect(action: "index")
    }


    def show(Alumno alumno){
        respond alumnoMostrar:alumno
    }




}
