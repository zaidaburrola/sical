package sical

class Alumno extends Usuario{
    String matricula
    String nomnbre
    String apellidoPaterno
    String apellidoMaterno
    String correo
    static hasOne=[calificacion:CalificacionGrupo]
    static hasMany = [grupo:Grupo]

    static constraints = {
        apellidoMaterno nullable: true
    }
}
