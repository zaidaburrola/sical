package sical

class CalificacionGrupo {

    static hasMany = [alumnos : Alumno, grupos : Grupo]
    int calificacion

    static constraints = {
        calificacion range: 0..100
    }
}
