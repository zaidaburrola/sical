package sical

class Grupo {

    String nombre
    Materia materia
    Maestro maestro

    static belongsTo = [Materia,Maestro]
    static mappedBy = [alumnos:Alumno]
    static constraints = {
        nombre nullable: false
    }
}
