package sical

class Maestro extends Usuario {

    String nombre
    String apellidoPaterno
    String apellidoMaterno
    String email
    int tipo
    String numEmpleado

    static constraints = {
        apellidoMaterno nullable: true
    }
}
