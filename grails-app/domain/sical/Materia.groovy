package sical

class Materia {
    String clave
    String nombre
    String plan
    static hasMany =[grupos:Grupo]

    static constraints = {
        clave nullable:false
        nombre nullable:false
        plan nullable:false
    }
}
